<?php
session_start();
include("connexion_projet.php");
$con = connect();
if (!$con) {
    echo "Problème de connexion à la base";
    exit;
}
?>
<!DOCTYPE html>
<html lang = "en">
<head>
    <meta charset = "UTF-8">
    <title> Espace Secrétaire </title>
    <link rel = "stylesheet" href = "../designe.css">
</head>
<body>
    <header>
        <h1> Espace Secrétaire </h1>
        <img src = "../img2.jpeg" id = "logo" >
        <img src = "../img2.jpeg" id = "logo2">
    </header>
    <nav>
        <ul>
            <li> <a href = "../accueil.html"> Accueil </a> </li>
            <li> <a href = "planning_inspecteur.php"> Voir le planning d'un inspecteur </a> </li>
            <li> <a href = "restant.php"> Voir ce qu'il reste à inspecter et à prélever </a> </li>
        </ul>
    </nav>
    <h2> Accès à votre espace personnel : </h2>
    <form action = "espace_perso_sec.php" method = POST>
        <select name = "secretaire[]">
            <?php
            $sql_sec = "SELECT noms
                        FROM secretaire" ;
            $resultat_sec = pg_query($sql_sec) ;
            if (!$resultat_sec) {
                echo "Problème lors du lancement de la requete";
                exit;
            }
            $ligne_sec = pg_fetch_array($resultat_sec);
            while ($ligne_sec) {
                echo "<option value = '".$ligne_sec['noms']."'>".$ligne_sec['noms']."</option>";
                $ligne_sec = pg_fetch_array($resultat_sec);
            }
            ?>
        </select> <br/> <br/>
        <input type = "submit" value = "Valider">
    </form>
</body>
</html>
