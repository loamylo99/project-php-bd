<?php
session_start();
$_SESSION['secretaire'] = $_POST['secretaire'][0];
include("connexion_projet.php");
$con = connect() ;
if (!$con) {
    echo "Problème de connexion à la base" ;
    exit ;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "UTF-8">
    <title> Espace Secrétaire </title>
    <link rel = "stylesheet" href = "../designe.css">
</head>
<body>
    <header>
        <h1> Espace Secrétaire </h1>
        <img src = "../img2.jpeg" id = "logo">
        <img src = "../img2.jpeg" id = "logo2">
    </header>
    <!-- Afficher le planning de l'inspecteur -->
    <nav>
        <ul>
            <li> <a href = "../accueil.html"> Accueil </a> </li>
            <li> <a href = "planning_inspecteur.php" target = "_blank"> Voir le planning d'un inspecteur </a> </li>
            <li> <a href = "restant.php" target = "_blank"> Voir ce qu'il reste à inspecter et prélever </a> </li>
        </ul>
    </nav>

    <h1> Votre espace personnel : </h1>

    <?php
    echo "Bonjour Mme ou M.  " .$_SESSION['secretaire']. "." ;
    ?>
    <br/> <br/>
    <form action = "choisir_exploitation.php" method = "POST">
        <h1> Ajout d'une inspection : </h1>
        Plante :
        <select name = "plante">
            <?php
            $sql_plt = "SELECT nomp
                        FROM typeplante" ;
            $resultat_plt = pg_query($sql_plt) ;
            echo $resultat_plt;
            if (!$resultat_plt) {
                echo "Problème lors du lancement de la requête" ;
                exit ;
            }
            $ligne_plt = pg_fetch_array($resultat_plt);
            while ($ligne_plt) {
                echo "<option value = '".$ligne_plt['nomp']."'>".$ligne_plt['nomp']."</option>";
                $ligne_plt = pg_fetch_array($resultat_plt);
            }
            ?>
        </select> <br/> <br/>
        Maladie :
        <select name = "maladie">
            <?php
            $sql_mal = "SELECT nommal
                        FROM maladie;";
            $resultat_mal = pg_query($sql_mal);

            if (!$resultat_mal) {
                echo "Problème lors du lancement de la requête" ;
                exit ;
            }
            $ligne_mal = pg_fetch_array($resultat_mal);
            while ($ligne_mal) {
                echo "<option value = '".$ligne_mal['nommal']."'>".$ligne_mal['nommal']."</option>";
                $ligne_mal = pg_fetch_array($resultat_mal);
            }
            ?>
        </select> <br/> <br/>
        <input type = "submit" value = "Valider">
        <br/> <br/>
    </form>
</body>
</html>
