<?php
session_start();
include("connexion_projet.php");
$con = connect();
if (!$con) {
    echo "Probleme connexion a la base";
    exit;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "UTF-8">
    <title> Planning Inspecteur </title>
    <link rel = "stylesheet" href = "../designe.css">
</head>
<body>
     <header>
        <h1> Planning Inspecteur </h1>
        <img src = "../img2.jpeg" id = "logo">
        <img src = "../img2.jpeg" id = "logo2">
    </header>
    <nav>
        <ul>
            <li> <a href = "accueil.html"> Accueil </a> </li>
        </ul>
    </nav>

    <h2> Choisir l'inspecteur sanitaire : </h2>
    <form action = "planning.php" method = "POST">
    <select id = 's' name = "inspecteurs[]">
        <?php
            $sql_ins = "SELECT nomi, prenomi
                        FROM inspecteur;";
            $resultat_ins = pg_query($sql_ins);
            if (!$resultat_ins) {
                echo "Probleme lors du lancement de la requete";
                exit;
            }
            $ligne_ins = pg_fetch_array($resultat_ins);
            while ($ligne_ins){
                echo "<option value = '".$ligne_ins['nomi']."'>".$ligne_ins['nomi']."</option>";
                $ligne_ins = pg_fetch_array($resultat_ins);
            }
        ?>
    </select> <br/> <br/>

    <input type = "submit" value = "Valider">
    </form>
</body>
</html>
