<?php
session_start();
$_SESSION['date'] = $_POST['date'];
$_SESSION['nombre'] = $_POST['nombre'];
include("connexion_projet.php");
$con = connect();
if (!$con) {
    echo "Problème connexion à la base";
    exit;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "UTF-8">
    <title> Espace Secrétaire </title>
    <link rel = "stylesheet" href = "../designe.css">
</head>
<body>
    <header>
        <h1> Espace Secrétaire </h1>
        <img src = "../img2.jpeg" id = "logo">
        <img src = "../img2.jpeg" id = "logo2">
    </header>
    <nav>
        <ul>
            <li> <a href = "../accueil.html"> Accueil </a> </li>
            <li> <a href = "planning_inspecteur.php" target = "_blank"> Voir le planning de l'inspecteur </a> </li>
        </ul>
    </nav>
    <form action = "ajouter_visite.php" method = "POST">
        Les inspecteurs :
        <?php
        $exists = "SELECT *
                   FROM visite
                   WHERE datev = '".$_SESSION['date']."'";
        $resExist = pg_query($exists);
        if ($resExist) {
            if (pg_num_rows($resExist) == 0) {
                for ($i = 0; $i < $_SESSION['nombre']; $i++) {
                    echo "<select name = 'inspecteur[]'>" ;
                    $sql_ins = "SELECT DISTINCT nomi, prenomi
                                FROM inspecteur
                                NATURAL JOIN visite AS v
                                WHERE v.datev != '" . $_SESSION['date'] . "'" ;
                    $resultat_ins = pg_query($sql_ins);
                    if (!$resultat_ins) {
                        echo "Problème lors du lancement de la requête";
                        exit;
                    }
                    $ligne_ins = pg_fetch_array($resultat_ins);
                    while ($ligne_ins) {
                        echo "<option value = '".$ligne_ins['nomi']."'>".$ligne_ins['nomi']."</option>";
                        $ligne_ins = pg_fetch_array($resultat_ins);
                    }
                    echo "</select> <br/> <br/>" ;
                }
                echo "Le nombre de plante à inspecter pour la visite :" ;
                $sql_nbMaxIns = "SELECT nbmaxinspecter
                                FROM cultiver
                                NATURAL JOIN typeplante
                                NATURAL JOIN exploitation
                                WHERE nomp = '" . $_SESSION['plante'] . "'
                                AND nome = '" . $_SESSION['exploitation'] . "'" ;
                $resultat_nbMaxIns = pg_query($sql_nbMaxIns);
                if (!$resultat_nbMaxIns) {
                    echo "Problème lors du lancement de la requête";
                    exit;
                }
                $ligne_nbMaxIns = pg_fetch_array($resultat_nbMaxIns);
                while ($ligne_nbMaxIns) {
                    echo "<input type = 'number' name = 'inspecter' max='" . $ligne_nbMaxIns['nbmaxinspecter'] . "'> <br/> <br/>" ;
                    $ligne_nbMaxIns = pg_fetch_array($resultat_nbMaxIns);
                }
                echo "Le nombre de plante à prélever pour la visite :";
                $sql_nbMaxPrel = "SELECT nbmaxprelever
                                FROM cultiver
                                NATURAL JOIN typeplante
                                NATURAL JOIN exploitation
                                WHERE nomp = '" . $_SESSION['plante'] . "'
                                AND nome = '" . $_SESSION['exploitation'] . "'" ;
                $resultat_nbMaxPrel = pg_query($sql_nbMaxPrel) ;
                if (!$resultat_nbMaxPrel) {
                    echo "Problème lors du lancement de la requête" ;
                    exit;
                }
                $ligne_nbMaxPrel = pg_fetch_array($resultat_nbMaxPrel) ;
                while ($ligne_nbMaxPrel) {
                    echo "<input type = 'number' name = 'prélever' max='" . $ligne_nbMaxPrel['nbmaxprelever'] . "'> <br/> <br/>" ;
                    $ligne_nbMaxPrel = pg_fetch_array($resultat_nbMaxPrel) ;
                }
            echo "L' identifiant de la visite : <input type = 'text' name = 'idv'> <br/>
            <input type = 'submit' value = 'Valider'>" ;
            }
            else {
                echo "Une visite a déjà lieu ce jour là. ";
            }
        }
        else {
            echo "Problème lors de la vérification de l'existence de la visite.";
        }
        ?>


    </form>
</body>
</html>
