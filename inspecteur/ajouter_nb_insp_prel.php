<?php
session_start();
$_SESSION['maladie']=$_GET['maladie'];
$_SESSION['typeplante']=$_GET['typeplante'];
$_SESSION['nb_inspections']=$_GET['nb_inspections'];
$_SESSION['nb_prelevements']=$_GET['nb_prelevements'];
include("connexion_projet.php");
$con = connect();
if (!$con) {
    echo "Problème de connexion à la base";
    exit;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "UTF-8">
    <title> Espace Inspectrice Sanitaire </title>
    <link rel = "stylesheet" href = "../designe.css">
</head>
<body>
<header>
    <h1>Espace Inspectrice Sanitaire </h1>
    <img src = "../img2.jpeg" id = "logo">
    <img src = "../img2.jpeg" id = "logo2">
</header>
<nav>
    <ul>
        <li> <a href = "../accueil.html"> Accueil </a> </li>
    </ul>
</nav>
<?php
    if ($_SERVER["REQUEST_METHOD"] == "GET") {
        $maladie = $_SESSION['maladie'];
        $typeplante = $_SESSION['typeplante'];
        $nb_inspections = $_SESSION['nb_inspections'];
        $nb_prelevements = $_SESSION['nb_prelevements'];
        $sqlIdm = "SELECT DISTINCT idm
                   FROM maladie
                   WHERE nommal='".$maladie."'";
        $resIdm = pg_query($sqlIdm);
        $rowIdm = pg_fetch_array($resIdm);
        $idm = $rowIdm['idm'];
        $sqlIdp = "SELECT DISTINCT idp
                   FROM typeplante
                   WHERE nomp='".$typeplante."'";
        $resIdp = pg_query($sqlIdp);
        $rowIdp = pg_fetch_array($resIdp);
        $idp = $rowIdp['idp'];
        $exists = "SELECT *
                   FROM plantemaladie
                   WHERE idm = '$idm' AND idp = '$idp'";
        $resExist = pg_query($exists);
        if ($resExist && pg_num_rows($resExist) != 0) {
            $ajout = "INSERT INTO resultat VALUES('".$idm."','".$idp."','".$_SESSION['idv']."',".$nb_inspections.",".$nb_prelevements.")";
            $resAjout = pg_query($ajout);
            if ($resAjout) {
                echo "<h3> Les données ont été ajoutées avec succès. </h3>";
            }
            else {
                echo "Cette paire maladie-plante a déjà subi une inspection. Vous ne pouvez pas ajouter de nouvelles inspections et prélèvements.";
            }
        }
        else {
            echo "Problème lors de la vérification de l'existence de la paire maladie-plante.";
        }
    }
 ?>
</body>
</html>
