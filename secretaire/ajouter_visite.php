<?php
session_start();
$_SESSION['inspecteur'] = $_POST['inspecteur'];
$_SESSION['inspecter'] = $_POST['inspecter'];
$_SESSION['prélever'] = $_POST['prélever'];
$_SESSION['idv'] = $_POST['idv'];
include("connexion_projet.php");
$con = connect();
if (!$con) {
    echo "Problème connexion à la base";
    exit;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "UTF-8">
    <title> Espace Secrétaire </title>
    <link rel = "stylesheet" href = "../designe.css">
</head>
<body>
    <header>
        <h1> Espace Secrétaire </h1>
        <img src = "../img2.jpeg" id = "logo">
        <img src = "../img2.jpeg" id = "logo2">
    </header>
    <nav>
        <ul>
            <li> <a href = "../accueil.html"> Accueil </a> </li>
            <li> <a href = "planning_inspecteur.php" target = "_blank"> Voir le planning d'un inspecteur </a> </li>
        </ul>
    </nav>
    <?php
    $idv = $_SESSION['idv'];
    $inspecter = $_SESSION['inspecter'];
    $prelever = $_SESSION['prélever'];
    $date = $_SESSION['date'];
    $sqlIde = "SELECT ide
               FROM exploitation
               WHERE nome='".$_SESSION['exploitation']."'";
    $resIde = pg_query($sqlIde);
    $rowIde = pg_fetch_array($resIde);
    $ide = $rowIde['ide'];
    $sqlIds = "SELECT ids
               FROM secretaire
               WHERE noms='".$_SESSION['secretaire']."'";
    $resIds = pg_query($sqlIds);
    $rowIds = pg_fetch_array($resIds);
    $ids = $rowIds['ids'];
    $exists = "SELECT *
               FROM visite
               WHERE idv = '$idv'";
    $resExist = pg_query($exists);
    if ($resExist) {
        if (pg_num_rows($resExist) == 0) {
            $ajout = "INSERT INTO visite VALUES ('".$idv."',".$inspecter.",".$prelever.",'".$date."','".$ide."','".$ids."')" ;
            $resAjout = pg_query($ajout);
            if ($resAjout) {
                echo "<h3> Les données ont été ajoutées avec succès. </h3>";
                echo "<table border=1> <tr>";
                for ($i = 1; $i <= $_SESSION['nombre']; $i++){
                    echo "<td>Insprecteur n°".$i."</td>";
                }
                echo "<td>Exploitation</td>
                      <td>Nombre Inspecter</td>
                      <td>Nombre Prelever</td>
                      <td>Secretaire</td>
                </tr>
                <tr> ";
                foreach ($_SESSION['inspecteur'] as $inspecteur) {
                    echo "<td>" . $inspecteur . "</td>";
                }
                echo "<td>".$_SESSION['exploitation']."</td>
                      <td>".$_SESSION['inspecter']."</td>
                      <td>".$_SESSION['prélever']."</td>
                      <td>".$_SESSION['secretaire']."</td>
                </tr>
                </table>";

                foreach ($_SESSION['inspecteur'] as $inspecteur) {

                            $sqlIdi = "SELECT idi
                                        FROM inspecteur
                                        WHERE nomi = '".$inspecteur."' ";
                            $resIdi = pg_query($sqlIdi);
                            $rowIdi = pg_fetch_array($resIdi);
                            $idi = $rowIdi['idi'];
                            $ajoutpart = "INSERT INTO participer VALUES ('".$idi."','".$idv."') on conflict (idi,idv) do nothing";
                            $resAjoutpart = pg_query($ajoutpart);
                }
            }
            else {
                echo "Problème lors de l'ajout des données.";
            }
        }
        else {
            echo "L'identifiant est déjà utilisé. ";
        }
    }
    else {
        echo "Problème lors de la vérification de l'existence de l'identifiant.";
    }
    ?>
</body>
</html>
