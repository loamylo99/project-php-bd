<?php
session_start();
$_SESSION['plante'] = $_POST['plante'];
$_SESSION['maladie'] = $_POST['maladie'];
include("connexion_projet.php");
$con = connect();
if (!$con) {
    echo "Problème de connexion à la base";
    exit;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "UTF-8">
    <title> Espace Gestionnaire </title>
    <link rel = "stylesheet" href = "../designe.css">
</head>
<body>
    <header>
        <h1> Espace Gestionnaire </h1>
        <img src = "../img2.jpeg" id = "logo">
        <img src = "../img2.jpeg" id = "logo2">
    </header>
    <nav>
        <ul>
            <li> <a href = "../accueil.html"> Accueil </a> </li>
        </ul>
    </nav>
    <table border = 1>
        <tr>
            <td> Plante </td>
            <td> Maladie </td>
            <td> Nombre total à inspecter </td>
            <td> Nombre total à prélever </td>
            <td> Nombre déjà à inspecter </td>
            <td> Nombre déjà à prélever </td>
        </tr>
        <tr>
        <?php
            echo '<td>' . $_SESSION['plante'] . '</td><td>' . $_SESSION['maladie'] . '</td>';
            $sql_nbTotalIns = "SELECT nbinitinspecter
                                FROM plantemaladie
                                NATURAL JOIN typeplante
                                NATURAL JOIN maladie
                                WHERE nomp = '" . $_SESSION['plante'] . "'
                                AND nommal = '" . $_SESSION['maladie'] . "'" ;
            $resultat_nbTotalIns = pg_query($sql_nbTotalIns);
            if (!$resultat_nbTotalIns) {
                echo "Problème lors du lancement de la requête";
                exit;
            }
            $ligne_nbTotalIns = pg_fetch_array($resultat_nbTotalIns);
            echo "<td>" . $ligne_nbTotalIns['nbinitinspecter'] . "</td>" ;
            $sql_nbTotalPrel = "SELECT nbinitprelever
                                FROM plantemaladie
                                NATURAL JOIN typeplante
                                NATURAL JOIN maladie
                                WHERE nomp = '" . $_SESSION['plante'] . "'
                                AND nommal = '" . $_SESSION['maladie'] . "'" ;
            $resultat_nbTotalPrel = pg_query($sql_nbTotalPrel);
            if (!$resultat_nbTotalPrel) {
                echo "Problème lors du lancement de la requête";
                exit;
            }
            $ligne_nbTotalPrel = pg_fetch_array($resultat_nbTotalPrel);
            echo "<td>" . $ligne_nbTotalPrel['nbinitprelever'] . "</td>" ;
            $sql_nbReelIns = "  SELECT
                                COALESCE(nbreelinspecter, 0) AS reelins
                                FROM resultat
                                NATURAL JOIN plantemaladie
                                NATURAL JOIN maladie
                                NATURAL JOIN typeplante
                                WHERE nomp = '" . $_SESSION['plante'] . "'
                                AND nommal = '" . $_SESSION['maladie'] . "'" ;
            $resultat_nbReelIns = pg_query($sql_nbReelIns);
            if (!$resultat_nbReelIns) {
                echo "Problème lors du lancement de la requête";
                exit;
            }
            $ligne_nbReelIns = pg_fetch_array($resultat_nbReelIns);
            $nbreelinspecter = isset($ligne_nbReelIns['nbreelinspecter']) ? $ligne_nbReelIns['nbreelinspecter'] : 0;
            echo "<td>" .$nbreelinspecter. "</td>" ;
            $sql_nbReelPrel = " SELECT nbreelprelever
                                FROM resultat
                                NATURAL JOIN plantemaladie
                                NATURAL JOIN typeplante
                                NATURAL JOIN maladie
                                WHERE nomp = '" . $_SESSION['plante'] . "'
                                AND nommal = '" . $_SESSION['maladie'] . "'" ;
            $resultat_nbReelPrel = pg_query($sql_nbReelPrel);
            if (!$resultat_nbReelPrel) {
                echo "Problème lors du lancement de la requête";
                exit;
            }
            $ligne_nbReelPrel = pg_fetch_array($resultat_nbReelPrel);
            $nbreelprelever = isset($ligne_nbReelPrel['nbreelprelever']) ? $ligne_nbReelIns['nbreelprelever'] : 0;
            echo "<td>" . $nbreelprelever ."</td>" ;
        ?>
        </tr>
    </table>
</body>
</html>
