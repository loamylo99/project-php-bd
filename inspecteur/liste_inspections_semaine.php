<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Donnees Inspection</title>
    <link rel="stylesheet" href="../designe.css">
</head>
<body>
     <header>
                <h1>Donnees Inspection</h1>

                <img src="../img2.jpeg" id="logo">
                <img src="../img2.jpeg" id="logo2">
    </header>
    <nav>
            <ul></ul>

    </nav>
<!--  CHATGPT  -->
    <?php
include 'connexion.php'; // Assurez-vous d'avoir un fichier de connexion à votre base de données PostgreSQL

$con = connect(); // La fonction connect doit être définie dans votre fichier de connexion

if (!$con) {
    echo "Problème de connexion à la base de données";
    exit;
}

// Supposons que vous ayez la date de début et de fin de la semaine
$dateDebut = "2023-11-01"; // Remplacez par la valeur réelle
$dateFin = "2023-11-07"; // Remplacez par la valeur réelle

// Récupérez la liste des inspections pour la semaine spécifiée
$sqlInspectionsSemaine = "SELECT idv, datev FROM inspections WHERE datev BETWEEN '$dateDebut' AND '$dateFin'";
$resInspectionsSemaine = pg_query($con, $sqlInspectionsSemaine);

// Vérifiez si la requête a réussi
if (!$resInspectionsSemaine) {
    echo "Problème lors de la récupération des inspections pour la semaine.";
    exit;
}

// Affichez la liste des inspections pour la semaine
echo "Liste des inspections pour la semaine du $dateDebut au $dateFin :<br>";
while ($rowInspection = pg_fetch_assoc($resInspectionsSemaine)) {
    echo "<a href='donner_liste_plantes.php?idv={$rowInspection['idv']}'>{$rowInspection['idv']} - {$rowInspection['datev']}</a><br>";
}

// Si un IDV spécifique est présent dans l'URL, affichez les détails de l'inspection
if (isset($_GET['idv'])) {
    $idv = $_GET['idv'];

    // Récupérez la liste des plantes inspectées et prélevées pour l'inspection spécifiée
    $sqlDetailsInspection = "SELECT idp, nbreelinspecter, nbreelprelever FROM votre_table WHERE idv = '$idv'";
    $resDetailsInspection = pg_query($con, $sqlDetailsInspection);

    // Vérifiez si la requête a réussi
    if (!$resDetailsInspection) {
        echo "Problème lors de la récupération des détails de l'inspection.";
        exit;
    }

    // Affichez la liste des plantes inspectées et prélevées
    echo "<br>Détails de l'inspection $idv :<br>";
    echo "<table border='1'>";
    echo "<tr><th>IDP</th><th>NbInspecté</th><th>NbPrélevé</th></tr>";
    while ($rowDetails = pg_fetch_assoc($resDetailsInspection)) {
        echo "<tr><td>{$rowDetails['idp']}</td><td>{$rowDetails['nbreelinspecter']}</td><td>{$rowDetails['nbreelprelever']}</td></tr>";
    }
    echo "</table>";
}

// Fermez la connexion à la base de données
pg_close($con);
?>



</body>
</html>

