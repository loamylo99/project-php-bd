<?php
session_start();
include("connexion_projet.php");
$con = connect() ;
if (!$con) {
    echo "Problème de connexion à la base" ;
    exit ;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "UTF-8">
    <title> Espace Secrétaire </title>
    <link rel = "stylesheet" href = "../designe.css">
</head>
<body>
    <header>
        <h1> Espace Secrétaire </h1>
        <img src = "../img2.jpeg" id = "logo">
        <img src = "../img2.jpeg" id = "logo2">
    </header>
    <nav>
        <ul>
            <li> <a href = "../accueil.html"> Accueil </a> </li>
            <li> <a href = "planning_inspecteur.php"> Voir le planning de l'inspecteur </a> </li>
        </ul>
    </nav>
    <h4> Liste des plantes et maladies qu'il reste à inspecter et à prélever : </h4> <br/>  <br/>
    <table border = 1>
        <tr>
            <td> Type de plante </td>
            <td> Maladie </td>
            <td> Reste à inspecter </td>
            <td> Reste à prélever </td>
        </tr>
    <?php
    $sql_ins = "SELECT p.nomp AS nomp, m.nommal AS nommal,
                CASE WHEN r.nbreelinspecter IS NOT NULL THEN  - r.nbreelinspecter
                ELSE pm.nbinitinspecter END AS resteinspecter,
                CASE WHEN r.nbreelprelever IS NOT NULL THEN  - r.nbreelprelever
                ELSE pm.nbinitprelever END AS resteprelever
                FROM typeplante AS p
                JOIN plantemaladie AS pm
                ON p.idp = pm.idp
                JOIN maladie AS m
                ON pm.idm = m.idm
                LEFT JOIN resultat AS r
                ON pm.idm = r.idm AND pm.idp = r.idp "; echo $sql_ins;


    $resultat_ins = pg_query($sql_ins);
    if (!$resultat_ins) {
        echo "Problème lors du lancement de la requête" ;
        exit ;
    }
    $ligne_ins = pg_fetch_array($resultat_ins);
            while ($ligne_ins) {
                echo "<tr> <td>". $ligne_ins['nomp']."</td>
                <td>". $ligne_ins['nommal']."</td>
                <td>". $ligne_ins['resteinspecter']."</td>
                <td>". $ligne_ins['resteprelever']."</td> </tr>";
                $ligne_ins = pg_fetch_array($resultat_ins);
            }
    ?>
    </table>
</body>
</html>
