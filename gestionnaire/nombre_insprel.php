<?php
session_start();
include("connexion_projet.php");
$con = connect();
if (!$con) {
    echo "Problème de connexion à la base";
    exit;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "UTF-8">
    <title> Espace Gestionnaire </title>
    <link rel = "stylesheet" href = "../designe.css">
</head>
<body>
    <header>
        <h1> Espace Gestionnaire </h1>
        <img src = "../img2.jpeg" id = "logo">
        <img src = "../img2.jpeg" id = "logo2">
    </header>
    <nav>
        <ul>
            <li> <a href = "../accueil.html"> Accueil </a> </li>
        </ul>
    </nav>
    <br/> <br/>
    <form action = "nombre_ajouter.php" method = "POST">
        <h1> Choisir la plante et la maladie : </h1>
        Plante :
        <select name = "plante">
            <?php
            $sql_plt = "SELECT nomp
                        FROM typeplante" ;
            $resultat_plt = pg_query($sql_plt) ;
            echo $resultat_plt;
            if (!$resultat_plt) {
                echo "Problème lors du lancement de la requête" ;
                exit ;
            }
            $ligne_plt = pg_fetch_array($resultat_plt);
            while ($ligne_plt) {
                echo "<option value = '".$ligne_plt['nomp']."'>".$ligne_plt['nomp']."</option>";
                $ligne_plt = pg_fetch_array($resultat_plt);
            }
            ?>
        </select> <br/> <br/>
        Maladie :
        <select name = "maladie">
            <?php
            $sql_mal = "SELECT nommal
                        FROM maladie;";
            $resultat_mal = pg_query($sql_mal);

            if (!$resultat_mal) {
                echo "Problème lors du lancement de la requête" ;
                exit ;
            }
            $ligne_mal = pg_fetch_array($resultat_mal);
            while ($ligne_mal) {
                echo "<option value = '".$ligne_mal['nommal']."'>".$ligne_mal['nommal']."</option>";
                $ligne_mal = pg_fetch_array($resultat_mal);
            }
            ?>
        </select> <br/> <br/>
        <input type = "submit" value = "Valider">
        <br/> <br/>
    </form>
</body>
</html>
