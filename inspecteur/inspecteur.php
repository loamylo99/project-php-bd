<?php
session_start();
include("connexion_projet.php");
$con = connect();
if (!$con) {
    echo "Problème de connexion à la base";
    exit;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset = "UTF-8">
        <title> Espace Inspecteur Sanitaire </title>
        <link rel = "stylesheet" href = "../designe.css">
    </head>
    <body>
        <header>
            <h1>Espace Inspecteur Sanitaire</h1>
            <img src = "../img2.jpeg" id = "logo">
            <img src = "../img2.jpeg" id = "logo2">
    </header>
    <nav>
        <ul>
            <li> <a href = "../accueil.html"> Accueil </a> </li>
        </ul>
    </nav>
    <h2> Accès à votre espace personnel : </h2>
    <form action = "espace_perso_insp.php" method = "GET">
        <select name = "inspecteurs[]">
            <?php
                $sql_insp = "select nomi from inspecteur";
                $resultat_insp = pg_query($sql_insp);
                if (!$resultat_insp)
                {
                    echo "Probleme lors du lancement de la requete";
                    exit;
                }
                $ligne_insp = pg_fetch_array($resultat_insp);
                while ($ligne_insp)
                {
                    echo "<option value = '".$ligne_insp['nomi']."'>".$ligne_insp['nomi']."</option>";
                    $ligne_insp = pg_fetch_array($resultat_insp);
                }
            ?>
        </select> <br/> <br/>
        <input type = "submit" value = "Valider">
    </form>
</body>
</html>
