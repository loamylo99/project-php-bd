<?php
session_start();
$_SESSION['inspecteurs'] = $_GET['inspecteurs'];
include("connexion_projet.php");
$con = connect();
if (!$con) {
    echo "Problème de connexion à la base";
    exit;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "UTF-8">
    <title> Espace Inspectrice Sanitaire </title>
    <link rel = "stylesheet" href = "../designe.css">
</head>
<body>
    <header>
        <h1> Espace Inspectrice Sanitaire </h1>
        <img src = "../img2.jpeg" id = "logo">
        <img src = "../img2.jpeg" id = "logo2">
    </header>
    <nav>
        <ul>
            <li> <a href = "../accueil.html"> Accueil </a> </li>
        </ul>
    </nav>

    <h1> Votre espace personnel : </h1>
    <?php
        echo " <h3> Bonjour Mme ou M.  " .$_SESSION['inspecteurs'][0].". </h3> " ;
    ?>
    <br/> <br/>
    <?php
        echo "<table border = 1> <tr> <td> Date </td> <td> Exploitation </td>";
        for($i = 0; $i < 7; $i++){
            echo "<tr>";
            $sql_date = "SELECT DATE(NOW()) + $i AS date" ;
            $resultat_date = pg_query($sql_date);
            if (!$resultat_date) {
                echo "Probleme lors du lancement de la requete";
                exit;
            }
            $ligne_date = pg_fetch_array($resultat_date);
            echo "<td>" . $ligne_date['date'] . " </td>";
            $sqlInspectionsSemaine = "SELECT v.idv as idv
                                      FROM visite AS v
                                      JOIN participer AS p
                                      ON v.idv = p.idv
                                      JOIN inspecteur AS i
                                      ON i.idi = p.idi
                                      WHERE datev = '".$ligne_date['date']."'
                                      AND i.nomi = '".$_SESSION['inspecteurs'][0]."' ";
            $resInspectionsSemaine = pg_query($sqlInspectionsSemaine);
            if ($resInspectionsSemaine) {
                if (pg_num_rows($resInspectionsSemaine) > 0) {
                    $rowInspection = pg_fetch_array($resInspectionsSemaine);
                    echo "<td> Visite <a href = 'donner_liste_plantes.php?idv={$rowInspection['idv']}'> {$rowInspection['idv']} </a><br> </td>";
                }
                else {
                    echo "<td>   </td>";
                }
            }
            else {
                echo "Problème lors de la récupération des inspections pour la semaine.";
                exit;
            }
            echo "</tr>";
        }
        echo "</table>";
    ?>
</body>
</html>
