<?php
session_start();
$_SESSION['inspecteurs'] = $_POST['inspecteurs'] ;
include("connexion_projet.php");
$con = connect();
if (!$con) {
    echo "Probleme connexion a la base";
    exit;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "UTF-8">
    <title> Planning Inspecteur </title>
    <link rel = "stylesheet" href = "../designe.css">
</head>
<body>
     <header>
        <h1> Planning Inspecteur </h1>
        <img src = "../img2.jpeg" id = "logo">
        <img src = "../img2.jpeg" id = "logo2">
    </header>
    <nav>
        <ul>
            <li> <a href = "../accueil.html"> Accueil </a> </li>
        </ul>
    </nav>
    <?php
        echo "<h3>Planning de Mme ou M.  " .$_SESSION['inspecteurs'][0]. ".</h3> <br>
        <table border = 1>
        <tr> <td> Jour </td>  <td> Exploitation à visiter </td> </tr>";
        for($i = 0; $i < 14; $i++){
            $sql_heure = "SELECT DATE(NOW()) + $i AS date" ;
            $resultat_heure = pg_query($sql_heure);
            if (!$resultat_heure) {
                echo "Probleme lors du lancement de la requete";
                exit;
            }
            $ligne_heure = pg_fetch_array($resultat_heure);
            $sql_exp = "SELECT nome
                        FROM exploitation AS e
                        JOIN visite AS v
                        ON e.ide = v.ide
                        JOIN participer AS p
                        ON v.idv = p.idv
                        JOIN inspecteur AS i
                        ON p.idi = i.idi
                        WHERE i.nomi = '".$_SESSION['inspecteurs'][0]."'
                        AND v.datev = '".$ligne_heure['date']."' " ;
            $resultat_exp = pg_query($sql_exp);
            if (!$resultat_exp) {
                echo "Probleme lors du lancement de la requete";
                exit;
            }
            $ligne_exp = pg_fetch_array($resultat_exp);
            echo "<tr> <td>".$ligne_heure['date']."</td> <td> ". ($ligne_exp ? $ligne_exp['nome'] : " ") ." </td> </tr>";
        }
        echo "</tr>
        </table>";
    ?>
</body>
</html>
