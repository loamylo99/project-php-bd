<?php
session_start();
$_SESSION['gestionnaire'][0] = $_POST['gestionnaire'][0];
include("connexion_projet.php");
$con = connect();
if (!$con) {
    echo "Problème de connexion à la base";
    exit;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "UTF-8">
    <title> Espace Gestionnaire </title>
    <link rel = "stylesheet" href = "../designe.css">
</head>
<body>
    <header>
        <h1> Espace Gestionnaire </h1>
        <img src = "../img2.jpeg" id = "logo">
        <img src = "../img2.jpeg" id = "logo2">
    </header>
    <nav>
        <ul>
            <li> <a href = "../accueil.html"> Accueil </a> </li>
            <li> <a href = "nombre_insprel.php"> Ajouter le nombre d'inspection et de prélèvement </a> </li>
        </ul>
    </nav>
    <h1> Votre espace personnel : </h1>
    <?php
        echo "Bonjour Mme ou M.  " .$_SESSION['gestionnaire'][0]."." ;
    ?>
    <br/> <br/>
    <form action = "nombre_plante.php" method = "POST">
        <h1> Accès aux nombres de plantes à inspecter et à prélever : </h1>
        Plante :
        <select name = "plante">
            <?php
            $sql_plt = "SELECT nomp
                        FROM typeplante" ;
            $resultat_plt = pg_query($sql_plt) ;
            echo $resultat_plt;
            if (!$resultat_plt) {
                echo "Problème lors du lancement de la requête" ;
                exit ;
            }
            $ligne_plt = pg_fetch_array($resultat_plt);
            while ($ligne_plt) {
                echo "<option value = '".$ligne_plt['nomp']."'>".$ligne_plt['nomp']."</option>";
                $ligne_plt = pg_fetch_array($resultat_plt);
            }
            ?>
        </select> <br/> <br/>
        Maladie :
        <select name = "maladie">
            <?php
            $sql_mal = "SELECT nommal
                        FROM maladie;";
            $resultat_mal = pg_query($sql_mal);

            if (!$resultat_mal) {
                echo "Problème lors du lancement de la requête" ;
                exit ;
            }
            $ligne_mal = pg_fetch_array($resultat_mal);
            while ($ligne_mal) {
                echo "<option value = '".$ligne_mal['nommal']."'>".$ligne_mal['nommal']."</option>";
                $ligne_mal = pg_fetch_array($resultat_mal);
            }
            ?>
        </select> <br/> <br/>
        <input type = "submit" value = "Valider">
        <br/> <br/>
    </form>
</body>
</html>
