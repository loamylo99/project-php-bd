<?php
session_start();
include("connexion_projet.php");
$con = connect();
if (!$con) {
    echo "Problème de connexion à la base";
    exit;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "UTF-8">
    <title> Espace Gestionnaire </title>
    <link rel = "stylesheet" href = "../designe.css">
</head>
<body>
     <header>
        <h1> Espace Gestionnaire </h1>
        <img src = "../img2.jpeg" id = "logo">
        <img src = "../img2.jpeg" id = "logo2">
    </header>
    <nav>
        <ul>
            <li> <a href = "../accueil.html"> Accueil </a> </li>
            <li> <a href = "../secretaire/secretaire.php"> Secrétaire </a> </li>
            <li> <a href = "../inspecteur/inspecteur.php"> Inspecteur Sanitaire </a> </li>
        </ul>
    </nav>
    <h2> Accès à votre espace personnel : </h2>
    <form action = "espace_perso_ges.php" method = "POST">
        <select name = "gestionnaire[]">
            <?php
            $sql_ges = "SELECT nomg
                        FROM gestionnaire" ;
            $resultat_ges = pg_query($sql_ges) ;
            if (!$resultat_ges) {
                echo "Problème lors du lancement de la requete";
                exit;
            }
            $ligne_ges = pg_fetch_array($resultat_ges);
            while ($ligne_ges) {
                echo "<option value = '".$ligne_ges['nomg']."'>".$ligne_ges['nomg']."</option>";
                $ligne_ges = pg_fetch_array($resultat_ges);
            }
            ?>
        </select> <br/> <br/>
        <input type = "submit" value = "Valider">
    </form>
</body>
</html>
